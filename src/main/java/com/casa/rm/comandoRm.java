package com.casa.comandoRm;

/**
*@author Cristian Nova 
*/
import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class comandoRm
{
	 public static void main(String args[])
	{
		JFrame frame = new JFrame("Sistema de eliminación de archivos y directorios");
		final JPanel panel1 = new JPanel(new GridLayout(3,3));
		JLabel jl1 = new JLabel("Selecciona el archivo o directorio que se va a eliminar");
		JLabel jl2 = new JLabel("Se ha seleccionado: ");
		final JFileChooser jfc = new JFileChooser();
		final JButton boton1 = new JButton("Seleccionar");
		final JButton boton2 = new JButton("Confirmar eliminación recursiva");
		final JTextField jtf = new JTextField();
		final JTextArea jta = new JTextArea();
		final JScrollPane scroll = new JScrollPane(jta);

		jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		frame.add(panel1);
		panel1.add(jl1);
		panel1.add(boton1);
		panel1.add(jl2);
		panel1.add(jtf);
		panel1.add(boton2);
		panel1.add(scroll);


		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(600,400));
		frame.setVisible(true);
		boton1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int seleccion = jfc.showOpenDialog(panel1);
				if (seleccion == JFileChooser.APPROVE_OPTION)
				{
					File archivoSeleccionado = jfc.getSelectedFile();
					jtf.setText(archivoSeleccionado.getPath());
				}
			}

		}); 
		boton2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				File archivo;
				archivo= new File(jtf.getText());
				if (archivo.exists())
				{
					jta.setText("Comprobando el archivo "+ jtf.getText());
					tipoDeArchivo(archivo,jta);
				}
				else
					jta.setText("El elemento "+ jtf.getText() + " no existe o ya ha sido eliminado.");
			}
		});
	}

	public static void tipoDeArchivo(File archivoF2, JTextArea jta2)
	{
		if (archivoF2.isDirectory())
		{
			jta2.append("\nEl elemento " + archivoF2.getName() + " es un directorio, entrando... ");
			esDirectorio(archivoF2,jta2);
		}
		else
		{
			jta2.append("\nEl elemento "+ archivoF2.getName() + " es un archivo y va a eliminarse...");
			archivoF2.delete();
		}
	}
	
	public static void esDirectorio(File archivoF3,JTextArea jta3)
	{
		File contenidos[]=archivoF3.listFiles();
		String nombres[]=archivoF3.list();
		for(int i=0;i<contenidos.length;i++)
		{
			jta3.append("\nEl elemento numero "+ (i+1) + " dentro del directorio " + archivoF3.getName() + " es " +nombres[i]);
			tipoDeArchivo(contenidos[i],jta3);
			archivoF3.delete();
		}
		jta3.append("\nEliminando "+archivoF3.getName());
		archivoF3.delete();
	}
	
}